/* Copyright (C) 2013, Cameron White
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package edu.pdx.cawhite.coordinates.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.pdx.cawhite.coordinates.*;
import edu.pdx.cawhite.math.coordinates.Coordinate;
import edu.pdx.cawhite.math.coordinates.CoordinateMatrix;

public class CoordinateMatrixTest {

	@Test
	public void testConstructor1() {
		CoordinateMatrix matrix = null;
		matrix = new CoordinateMatrix(2, 2);
		assertTrue("Item (0,0) should be 0.0", 
			matrix.get(0,0) == 0.0);
		assertTrue("Item (0,1) should be 0.0", 
			matrix.get(0,0) == 0.0);
		assertTrue("Item (1,1) should be 1.0", 
			matrix.get(1,0) == 1.0);
		assertTrue("Item (1,1) should be 1.0", 
			matrix.get(1,1) == 1.0);
	}
	
	@Test
	public void testConstructor2() {
		CoordinateMatrix matrix = null;
		matrix = new CoordinateMatrix(3, 3);
		assertTrue("Item (0,0) should be 0.0", 
			matrix.get(0,0) == 0.0);
		assertTrue("Item (0,1) should be 0.0", 
			matrix.get(0,1) == 0.0);
		assertTrue("Item (0,2) should be 0.0", 
			matrix.get(0,2) == 0.0);
		assertTrue("Item (1,0) should be 0.0", 
			matrix.get(1,0) == 0.0);
		assertTrue("Item (1,1) should be 0.0", 
			matrix.get(1,1) == 0.0);
		assertTrue("Item (1,2) should be 0.0", 
			matrix.get(1,2) == 0.0);
		assertTrue("Item (2,0) should be 1.0", 
			matrix.get(2,0) == 1.0);
		assertTrue("Item (2,1) should be 1.0", 
			matrix.get(2,1) == 1.0);
		assertTrue("Item (2,2) should be 1.0", 
			matrix.get(2,2) == 1.0);
	}

	@Test
	public void testGetCoordinate1() {
		CoordinateMatrix matrix = new CoordinateMatrix(2,2);
		Coordinate coordinate0 = matrix.getCoordinate(0);
		Coordinate coordinate1 = matrix.getCoordinate(1);
		assertTrue("Item 0 should be 0", coordinate0.get(0) == 0.0);
		assertTrue("Item 1 should be 1", coordinate0.get(1) == 1.0);
		assertTrue("Item 0 should be 0", coordinate1.get(0) == 0.0);
		assertTrue("Item 1 should be 1", coordinate1.get(1) == 1.0);
	}

}
