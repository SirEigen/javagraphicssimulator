/* Copyright (C) 2013, Cameron White
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package edu.pdx.cawhite.math.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.pdx.cawhite.math.RowMatrix;
import edu.pdx.cawhite.math.Vector;
import edu.pdx.cawhite.math.MatrixSizeException;

/**
 * @author Cameron Brandon White
 */
public class RowMatrixTest {

	
	@Test
	public void testConstructor1by1() {
		RowMatrix matrix = new RowMatrix(1, 1);
		assertTrue("The matrix should have 1 row.", 
				matrix.getNumberOfRows() == 1);
		assertTrue("The matrix should have 1 column.", 
				matrix.getNumberOfColumns() == 1);
		assertTrue("The item should be 0", 
				matrix.get(0,0) == 0);
	}
	
	@Test
	public void testConstructor2by2() {
		RowMatrix matrix = new RowMatrix(2, 2);
		assertTrue("The matrix should have 1 row.", 
				matrix.getNumberOfRows() == 2);
		assertTrue("The matrix should have 1 column.", 
				matrix.getNumberOfColumns() == 2);
		assertTrue("The item (0,0) should be 0.",
				matrix.get(0,0) == 0);
		assertTrue("The item (0,1) should be 0.",
				matrix.get(0,1) == 0);
		assertTrue("The item (1,0) should be 0.",
				matrix.get(1,0) == 0);
		assertTrue("The item (1,1) should be 0.",
				matrix.get(1,1) == 0);
	}
	
	@Test
	public void testConstructor3by3() {
		RowMatrix matrix = new RowMatrix(3, 3);
		assertTrue("The matrix should have 1 row.", 
				matrix.getNumberOfRows() == 3);
		assertTrue("The matrix should have 1 column.", 
				matrix.getNumberOfColumns() == 3);
		assertTrue("The item (0,0) should be 0.",
				matrix.get(0,0) == 0);
		assertTrue("The item (0,1) should be 0.",
				matrix.get(0,1) == 0);
		assertTrue("The item (0,2) should be 0.",
				matrix.get(0,2) == 0);
		assertTrue("The item (1,0) should be 0.",
				matrix.get(1,0) == 0);
		assertTrue("The item (1,1) should be 0.",
				matrix.get(1,1) == 0);
		assertTrue("The item (1,2) should be 0.",
				matrix.get(1,2) == 0);
		assertTrue("The item (2,0) should be 0.",
				matrix.get(2,0) == 0);
		assertTrue("The item (2,1) should be 0.",
				matrix.get(2,1) == 0);
		assertTrue("The item (2,2) should be 0.",
				matrix.get(2,2) == 0);
	}
	
	@Test
	public void test1by1SetGet() {
		RowMatrix matrix = new RowMatrix(1, 1);
		matrix.set(0, 0, 1.0);
		assertTrue("The item should be 1",
				matrix.get(0, 0) == 1);
	}
	
	@Test
	public void test2by2SetGet() {
		RowMatrix matrix = new RowMatrix(2, 2);
		matrix.set(0, 0, 1.0);
		matrix.set(0, 1, 2.0);
		matrix.set(1, 0, 3.0);
		matrix.set(1, 1, 4.0);
		assertTrue("The item (0,0) should be 1.0.",
				matrix.get(0,0) == 1.0);
		assertTrue("The item (0,1) should be 2.0.",
				matrix.get(0,1) == 2.0);
		assertTrue("The item (1,0) should be 3.0.",
				matrix.get(1,0) == 3.0);
		assertTrue("The item (1,1) should be 4.0.",
				matrix.get(1,1) == 4.0);
	}
	
	@Test 
	public void test3by3SetGet() {
		RowMatrix matrix = new RowMatrix(3, 3);
		matrix.set(0, 0, 1.0);
		matrix.set(0, 1, 2.0);
		matrix.set(0, 2, 3.0);
		matrix.set(1, 0, 4.0);
		matrix.set(1, 1, 5.0);
		matrix.set(1, 2, 6.0);
		matrix.set(2, 0, 7.0);
		matrix.set(2, 1, 8.0);
		matrix.set(2, 2, 9.0);
		assertTrue("The item (0,0) should be 1.0.",
				matrix.get(0,0) == 1.0);
		assertTrue("The item (0,1) should be 2.0.",
				matrix.get(0,1) == 2.0);
		assertTrue("The item (0,2) should be 3.0.",
				matrix.get(0,2) == 3.0);
		assertTrue("The item (1,0) should be 4.0.",
				matrix.get(1,0) == 4.0);
		assertTrue("The item (1,1) should be 5.0.",
				matrix.get(1,1) == 5.0);
		assertTrue("The item (1,2) should be 6.0.",
				matrix.get(1,2) == 6.0);
		assertTrue("The item (2,0) should be 7.0.",
				matrix.get(2,0) == 7.0);
		assertTrue("The item (2,1) should be 8.0.",
				matrix.get(2,1) == 8.0);
		assertTrue("The item (2,2) should be 9.0.",
				matrix.get(2,2) == 9.0);
	}
	
	@Test
	public void test1by1Add() {
		try {
			RowMatrix matrix0 = new RowMatrix(1,1);
			RowMatrix matrix1 = new RowMatrix(1,1);
			matrix0.set(0, 0, 1.0);
			matrix1.set(0, 0, 2.0);
			RowMatrix matrix2 = matrix0.add(matrix1);
			assertTrue("The item should be 1.0",
					matrix0.get(0, 0) == 1.0);
			assertTrue("The item should be 2.0",
					matrix1.get(0, 0) == 2.0);
			assertTrue("The item should be 3.0",
					matrix2.get(0, 0) ==  3.0);
		} catch (MatrixSizeException e) {
			fail();
		}
	}
	
	@Test
	public void test1by1IAdd() {
		try {
			RowMatrix matrix0 = new RowMatrix(1,1);
			RowMatrix matrix1 = new RowMatrix(1,1);
			matrix0.set(0, 0, 1.0);
			matrix1.set(0, 0, 2.0);
			matrix0.iadd(matrix1);
			assertTrue("The item should be 3.0",
					matrix0.get(0, 0) == 3.0);
			assertTrue("The item should be 2.0",
					matrix1.get(0, 0) == 2.0);
		} catch (MatrixSizeException e) {
			fail();
		}
	}
	
	@Test
	public void test2by2Add() {
		try {
			RowMatrix matrix0 = new RowMatrix(2,2);
			RowMatrix matrix1 = new RowMatrix(2,2);
			matrix0.set(0, 0, 1.0);
			matrix0.set(0, 1, 2.0);
			matrix0.set(1, 0, 3.0);
			matrix0.set(1, 1, 4.0);
			matrix1.set(0, 0, 5.0);
			matrix1.set(0, 1, 6.0);
			matrix1.set(1, 0, 7.0);
			matrix1.set(1, 1, 8.0);
			RowMatrix matrix2 = matrix0.add(matrix1);
			assertTrue("The item (0,0) should be 1.0.",
					matrix0.get(0,0) == 1.0);
			assertTrue("The item (0,1) should be 2.0.",
					matrix0.get(0,1) == 2.0);
			assertTrue("The item (1,0) should be 3.0.",
					matrix0.get(1,0) == 3.0);
			assertTrue("The item (1,1) should be 4.0.",
					matrix0.get(1,1) == 4.0);
			assertTrue("The item (0,0) should be 5.0.",
					matrix1.get(0,0) == 5.0);
			assertTrue("The item (0,1) should be 6.0.",
					matrix1.get(0,1) == 6.0);
			assertTrue("The item (1,0) should be 7.0.",
					matrix1.get(1,0) == 7.0);
			assertTrue("The item (1,1) should be 8.0.",
					matrix1.get(1,1) == 8.0);
			assertTrue("The item (0,0) should be 6.0.",
					matrix2.get(0,0) == 6.0);
			assertTrue("The item (0,1) should be 8.0.",
					matrix2.get(0,1) == 8.0);
			assertTrue("The item (1,0) should be 10.0.",
					matrix2.get(1,0) == 10.0);
			assertTrue("The item (1,1) should be 12.0.",
					matrix2.get(1,1) == 12.0);
		} catch (MatrixSizeException e) {
			fail();
		}
	}
	
	@Test
	public void test2by2IAdd() {
		try {
			RowMatrix matrix0 = new RowMatrix(2,2);
			RowMatrix matrix1 = new RowMatrix(2,2);
			matrix0.set(0, 0, 1.0);
			matrix0.set(0, 1, 2.0);
			matrix0.set(1, 0, 3.0);
			matrix0.set(1, 1, 4.0);
			matrix1.set(0, 0, 5.0);
			matrix1.set(0, 1, 6.0);
			matrix1.set(1, 0, 7.0);
			matrix1.set(1, 1, 8.0);
			matrix0.iadd(matrix1);
			assertTrue("The item (0,0) should be 6.0.",
					matrix0.get(0,0) == 6.0);
			assertTrue("The item (0,1) should be 8.0.",
					matrix0.get(0,1) == 8.0);
			assertTrue("The item (1,0) should be 10.0.",
					matrix0.get(1,0) == 10.0);
			assertTrue("The item (1,1) should be 12.0.",
					matrix0.get(1,1) == 12.0);
			assertTrue("The item (0,0) should be 5.0.",
					matrix1.get(0,0) == 5.0);
			assertTrue("The item (0,1) should be 6.0.",
					matrix1.get(0,1) == 6.0);
			assertTrue("The item (1,0) should be 7.0.",
					matrix1.get(1,0) == 7.0);
			assertTrue("The item (1,1) should be 8.0.",
					matrix1.get(1,1) == 8.0);
		} catch (MatrixSizeException e) {
			fail();
		}
	}
	
	@Test
	public void test3by3Add() {
		try {
			RowMatrix matrix0 = new RowMatrix(3,3);
			RowMatrix matrix1 = new RowMatrix(3,3);
			matrix0.set(0, 0, 1.0);
			matrix0.set(0, 1, 2.0);
			matrix0.set(0, 2, 3.0);
			matrix0.set(1, 0, 4.0);
			matrix0.set(1, 1, 5.0);
			matrix0.set(1, 2, 6.0);
			matrix0.set(2, 0, 7.0);
			matrix0.set(2, 1, 8.0);
			matrix0.set(2, 2, 9.0);
			
			matrix1.set(0, 0, 10.0);
			matrix1.set(0, 1, 11.0);
			matrix1.set(0, 2, 12.0);
			matrix1.set(1, 0, 13.0);
			matrix1.set(1, 1, 14.0);
			matrix1.set(1, 2, 15.0);
			matrix1.set(2, 0, 16.0);
			matrix1.set(2, 1, 17.0);
			matrix1.set(2, 2, 18.0);
			
			RowMatrix matrix2 = matrix0.add(matrix1);
			
			assertTrue("The item (0,0) should be 1.0.",
					matrix0.get(0,0) == 1.0);
			assertTrue("The item (0,1) should be 2.0.",
					matrix0.get(0,1) == 2.0);
			assertTrue("The item (0,2) should be 3.0.",
					matrix0.get(0,2) == 3.0);
			assertTrue("The item (1,0) should be 4.0.",
					matrix0.get(1,0) == 4.0);
			assertTrue("The item (1,1) should be 5.0.",
					matrix0.get(1,1) == 5.0);
			assertTrue("The item (1,2) should be 6.0.",
					matrix0.get(1,2) == 6.0);
			assertTrue("The item (2,0) should be 7.0.",
					matrix0.get(2,0) == 7.0);
			assertTrue("The item (2,1) should be 8.0.",
					matrix0.get(2,1) == 8.0);
			assertTrue("The item (2,2) should be 9.0.",
					matrix0.get(2,2) == 9.0);
			
			assertTrue("The item (0,0) should be 10.0.",
					matrix1.get(0,0) == 10.0);
			assertTrue("The item (0,1) should be 11.0.",
					matrix1.get(0,1) == 11.0);
			assertTrue("The item (0,2) should be 12.0.",
					matrix1.get(0,2) == 12.0);
			assertTrue("The item (1,0) should be 13.0.",
					matrix1.get(1,0) == 13.0);
			assertTrue("The item (1,1) should be 14.0.",
					matrix1.get(1,1) == 14.0);
			assertTrue("The item (1,2) should be 15.0.",
					matrix1.get(1,2) == 15.0);
			assertTrue("The item (2,0) should be 16.0.",
					matrix1.get(2,0) == 16.0);
			assertTrue("The item (2,1) should be 17.0.",
					matrix1.get(2,1) == 17.0);
			assertTrue("The item (2,2) should be 18.0.",
					matrix1.get(2,2) == 18.0);

			assertTrue("The item (0,0) should be 11.0.",
					matrix2.get(0,0) == 11.0);
			assertTrue("The item (0,1) should be 13.0.",
					matrix2.get(0,1) == 13.0);
			assertTrue("The item (0,2) should be 15.0.",
					matrix2.get(0,2) == 15.0);
			assertTrue("The item (1,0) should be 17.0.",
					matrix2.get(1,0) == 17.0);
			assertTrue("The item (1,1) should be 19.0.",
					matrix2.get(1,1) == 19.0);
			assertTrue("The item (1,2) should be 21.0.",
					matrix2.get(1,2) == 21.0);
			assertTrue("The item (2,0) should be 23.0.",
					matrix2.get(2,0) == 23.0);
			assertTrue("The item (2,1) should be 25.0.",
					matrix2.get(2,1) == 25.0);
			assertTrue("The item (2,2) should be 27.0.",
					matrix2.get(2,2) == 27.0);
		} catch (MatrixSizeException e) {
			fail();
		}
	}
	
	@Test
	public void test3by3IAdd() {
		try {
			RowMatrix matrix0 = new RowMatrix(3,3);
			RowMatrix matrix1 = new RowMatrix(3,3);
			
			matrix0.set(0, 0, 1.0);
			matrix0.set(0, 1, 2.0);
			matrix0.set(0, 2, 3.0);
			matrix0.set(1, 0, 4.0);
			matrix0.set(1, 1, 5.0);
			matrix0.set(1, 2, 6.0);
			matrix0.set(2, 0, 7.0);
			matrix0.set(2, 1, 8.0);
			matrix0.set(2, 2, 9.0);
			
			matrix1.set(0, 0, 10.0);
			matrix1.set(0, 1, 11.0);
			matrix1.set(0, 2, 12.0);
			matrix1.set(1, 0, 13.0);
			matrix1.set(1, 1, 14.0);
			matrix1.set(1, 2, 15.0);
			matrix1.set(2, 0, 16.0);
			matrix1.set(2, 1, 17.0);
			matrix1.set(2, 2, 18.0);
			
			matrix0.iadd(matrix1);
			
			assertTrue("The item (0,0) should be 11.0.",
					matrix0.get(0,0) == 11.0);
			assertTrue("The item (0,1) should be 13.0.",
					matrix0.get(0,1) == 13.0);
			assertTrue("The item (0,2) should be 15.0.",
					matrix0.get(0,2) == 15.0);
			assertTrue("The item (1,0) should be 17.0.",
					matrix0.get(1,0) == 17.0);
			assertTrue("The item (1,1) should be 19.0.",
					matrix0.get(1,1) == 19.0);
			assertTrue("The item (1,2) should be 21.0.",
					matrix0.get(1,2) == 21.0);
			assertTrue("The item (2,0) should be 23.0.",
					matrix0.get(2,0) == 23.0);
			assertTrue("The item (2,1) should be 25.0.",
					matrix0.get(2,1) == 25.0);
			assertTrue("The item (2,2) should be 27.0.",
					matrix0.get(2,2) == 27.0);
			
			assertTrue("The item (0,0) should be 10.0.",
					matrix1.get(0,0) == 10.0);
			assertTrue("The item (0,1) should be 11.0.",
					matrix1.get(0,1) == 11.0);
			assertTrue("The item (0,2) should be 12.0.",
					matrix1.get(0,2) == 12.0);
			assertTrue("The item (1,0) should be 13.0.",
					matrix1.get(1,0) == 13.0);
			assertTrue("The item (1,1) should be 14.0.",
					matrix1.get(1,1) == 14.0);
			assertTrue("The item (1,2) should be 15.0.",
					matrix1.get(1,2) == 15.0);
			assertTrue("The item (2,0) should be 16.0.",
					matrix1.get(2,0) == 16.0);
			assertTrue("The item (2,1) should be 17.0.",
					matrix1.get(2,1) == 17.0);
			assertTrue("The item (2,2) should be 18.0.",
					matrix1.get(2,2) == 18.0);
		} catch (MatrixSizeException e) {
			fail();
		}
	}
	
	@Test
	public void test1by1Sub() {
		try {
			RowMatrix matrix0 = new RowMatrix(1,1);
			RowMatrix matrix1 = new RowMatrix(1,1);
			matrix0.set(0, 0, 1.0);
			matrix1.set(0, 0, 2.0);
			RowMatrix matrix2 = matrix0.sub(matrix1);
			assertTrue("The item should be 1.0",
					matrix0.get(0, 0) == 1.0);
			assertTrue("The item should be 2.0",
					matrix1.get(0, 0) == 2.0);
			assertTrue("The item should be -1.0",
					matrix2.get(0, 0) ==  -1.0);
		} catch (MatrixSizeException e) {
			fail();
		}
	}
	
	@Test
	public void test1by1ISub() {
		try {
			RowMatrix matrix0 = new RowMatrix(1,1);
			RowMatrix matrix1 = new RowMatrix(1,1);
			matrix0.set(0, 0, 1.0);
			matrix1.set(0, 0, 2.0);
			matrix0.isub(matrix1);
			assertTrue("The item should be -1.0",
					matrix0.get(0, 0) == -1.0);
			assertTrue("The item should be 2.0",
					matrix1.get(0, 0) == 2.0);
		} catch (MatrixSizeException e) {
			fail();
		}
	}
	
	@Test
	public void test2by2Sub() {
		try {
			RowMatrix matrix0 = new RowMatrix(2,2);
			RowMatrix matrix1 = new RowMatrix(2,2);
			
			matrix0.set(0, 0, 1.0);
			matrix0.set(0, 1, 2.0);
			matrix0.set(1, 0, 3.0);
			matrix0.set(1, 1, 4.0);
			
			matrix1.set(0, 0, 5.0);
			matrix1.set(0, 1, 6.0);
			matrix1.set(1, 0, 7.0);
			matrix1.set(1, 1, 8.0);
			
			RowMatrix matrix2 = matrix0.sub(matrix1);
			
			assertTrue("The item (0,0) should be 1.0.",
					matrix0.get(0,0) == 1.0);
			assertTrue("The item (0,1) should be 2.0.",
					matrix0.get(0,1) == 2.0);
			assertTrue("The item (1,0) should be 3.0.",
					matrix0.get(1,0) == 3.0);
			assertTrue("The item (1,1) should be 4.0.",
					matrix0.get(1,1) == 4.0);
			
			assertTrue("The item (0,0) should be 5.0.",
					matrix1.get(0,0) == 5.0);
			assertTrue("The item (0,1) should be 6.0.",
					matrix1.get(0,1) == 6.0);
			assertTrue("The item (1,0) should be 7.0.",
					matrix1.get(1,0) == 7.0);
			assertTrue("The item (1,1) should be 8.0.",
					matrix1.get(1,1) == 8.0);
			
			assertTrue("The item (0,0) should be -4.0.",
					matrix2.get(0,0) == -4.0);
			assertTrue("The item (0,1) should be -4.0.",
					matrix2.get(0,1) == -4.0);
			assertTrue("The item (1,0) should be -4.0.",
					matrix2.get(1,0) == -4.0);
			assertTrue("The item (1,1) should be -4.0.",
					matrix2.get(1,1) == -4.0);
		} catch (MatrixSizeException e) {
			fail();
		}
	}
	
	@Test
	public void test2by2ISub() {
		try {
			RowMatrix matrix0 = new RowMatrix(2,2);
			RowMatrix matrix1 = new RowMatrix(2,2);
			
			matrix0.set(0, 0, 1.0);
			matrix0.set(0, 1, 2.0);
			matrix0.set(1, 0, 3.0);
			matrix0.set(1, 1, 4.0);
			
			matrix1.set(0, 0, 5.0);
			matrix1.set(0, 1, 6.0);
			matrix1.set(1, 0, 7.0);
			matrix1.set(1, 1, 8.0);
			
			matrix0.isub(matrix1);
			
			assertTrue("The item (0,0) should be -4.0.",
					matrix0.get(0,0) == -4.0);
			assertTrue("The item (0,1) should be -4.0.",
					matrix0.get(0,1) == -4.0);
			assertTrue("The item (1,0) should be -4.0.",
					matrix0.get(1,0) == -4.0);
			assertTrue("The item (1,1) should be -4.0.",
					matrix0.get(1,1) == -4.0);
			
			assertTrue("The item (0,0) should be 5.0.",
					matrix1.get(0,0) == 5.0);
			assertTrue("The item (0,1) should be 6.0.",
					matrix1.get(0,1) == 6.0);
			assertTrue("The item (1,0) should be 7.0.",
					matrix1.get(1,0) == 7.0);
			assertTrue("The item (1,1) should be 8.0.",
					matrix1.get(1,1) == 8.0);
		} catch (MatrixSizeException e) {
			fail();
		}
	}
	
	@Test
	public void test3by3Sub() {
		try {
			RowMatrix matrix0 = new RowMatrix(3,3);
			RowMatrix matrix1 = new RowMatrix(3,3);
			matrix0.set(0, 0, 1.0);
			matrix0.set(0, 1, 2.0);
			matrix0.set(0, 2, 3.0);
			matrix0.set(1, 0, 4.0);
			matrix0.set(1, 1, 5.0);
			matrix0.set(1, 2, 6.0);
			matrix0.set(2, 0, 7.0);
			matrix0.set(2, 1, 8.0);
			matrix0.set(2, 2, 9.0);
			
			matrix1.set(0, 0, 10.0);
			matrix1.set(0, 1, 11.0);
			matrix1.set(0, 2, 12.0);
			matrix1.set(1, 0, 13.0);
			matrix1.set(1, 1, 14.0);
			matrix1.set(1, 2, 15.0);
			matrix1.set(2, 0, 16.0);
			matrix1.set(2, 1, 17.0);
			matrix1.set(2, 2, 18.0);
			
			RowMatrix matrix2 = matrix0.sub(matrix1);
			
			assertTrue("The item (0,0) should be 1.0.",
					matrix0.get(0,0) == 1.0);
			assertTrue("The item (0,1) should be 2.0.",
					matrix0.get(0,1) == 2.0);
			assertTrue("The item (0,2) should be 3.0.",
					matrix0.get(0,2) == 3.0);
			assertTrue("The item (1,0) should be 4.0.",
					matrix0.get(1,0) == 4.0);
			assertTrue("The item (1,1) should be 5.0.",
					matrix0.get(1,1) == 5.0);
			assertTrue("The item (1,2) should be 6.0.",
					matrix0.get(1,2) == 6.0);
			assertTrue("The item (2,0) should be 7.0.",
					matrix0.get(2,0) == 7.0);
			assertTrue("The item (2,1) should be 8.0.",
					matrix0.get(2,1) == 8.0);
			assertTrue("The item (2,2) should be 9.0.",
					matrix0.get(2,2) == 9.0);
			
			assertTrue("The item (0,0) should be 10.0.",
					matrix1.get(0,0) == 10.0);
			assertTrue("The item (0,1) should be 11.0.",
					matrix1.get(0,1) == 11.0);
			assertTrue("The item (0,2) should be 12.0.",
					matrix1.get(0,2) == 12.0);
			assertTrue("The item (1,0) should be 13.0.",
					matrix1.get(1,0) == 13.0);
			assertTrue("The item (1,1) should be 14.0.",
					matrix1.get(1,1) == 14.0);
			assertTrue("The item (1,2) should be 15.0.",
					matrix1.get(1,2) == 15.0);
			assertTrue("The item (2,0) should be 16.0.",
					matrix1.get(2,0) == 16.0);
			assertTrue("The item (2,1) should be 17.0.",
					matrix1.get(2,1) == 17.0);
			assertTrue("The item (2,2) should be 18.0.",
					matrix1.get(2,2) == 18.0);

			assertTrue("The item (0,0) should be -9.0.",
					matrix2.get(0,0) == -9.0);
			assertTrue("The item (0,1) should be -9.0.",
					matrix2.get(0,1) == -9.0);
			assertTrue("The item (0,2) should be -9.0.",
					matrix2.get(0,2) == -9.0);
			assertTrue("The item (1,0) should be -9.0.",
					matrix2.get(1,0) == -9.0);
			assertTrue("The item (1,1) should be -9.0.",
					matrix2.get(1,1) == -9.0);
			assertTrue("The item (1,2) should be -9.0.",
					matrix2.get(1,2) == -9.0);
			assertTrue("The item (2,0) should be -9.0.",
					matrix2.get(2,0) == -9.0);
			assertTrue("The item (2,1) should be -9.0.",
					matrix2.get(2,1) == -9.0);
			assertTrue("The item (2,2) should be -9.0.",
					matrix2.get(2,2) == -9.0);
		} catch (MatrixSizeException e) {
			fail();
		}
	}
	
	@Test
	public void test3by3ISub() {
		try {
			RowMatrix matrix0 = new RowMatrix(3,3);
			RowMatrix matrix1 = new RowMatrix(3,3);
			
			matrix0.set(0, 0, 1.0);
			matrix0.set(0, 1, 2.0);
			matrix0.set(0, 2, 3.0);
			matrix0.set(1, 0, 4.0);
			matrix0.set(1, 1, 5.0);
			matrix0.set(1, 2, 6.0);
			matrix0.set(2, 0, 7.0);
			matrix0.set(2, 1, 8.0);
			matrix0.set(2, 2, 9.0);
			
			matrix1.set(0, 0, 10.0);
			matrix1.set(0, 1, 11.0);
			matrix1.set(0, 2, 12.0);
			matrix1.set(1, 0, 13.0);
			matrix1.set(1, 1, 14.0);
			matrix1.set(1, 2, 15.0);
			matrix1.set(2, 0, 16.0);
			matrix1.set(2, 1, 17.0);
			matrix1.set(2, 2, 18.0);
			
			matrix0.isub(matrix1);
			
			assertTrue("The item (0,0) should be -9.0.",
					matrix0.get(0,0) == -9.0);
			assertTrue("The item (0,1) should be -9.0.",
					matrix0.get(0,1) == -9.0);
			assertTrue("The item (0,2) should be -9.0.",
					matrix0.get(0,2) == -9.0);
			assertTrue("The item (1,0) should be -9.0.",
					matrix0.get(1,0) == -9.0);
			assertTrue("The item (1,1) should be -9.0.",
					matrix0.get(1,1) == -9.0);
			assertTrue("The item (1,2) should be -9.0.",
					matrix0.get(1,2) == -9.0);
			assertTrue("The item (2,0) should be -9.0.",
					matrix0.get(2,0) == -9.0);
			assertTrue("The item (2,1) should be -9.0.",
					matrix0.get(2,1) == -9.0);
			assertTrue("The item (2,2) should be -9.0.",
					matrix0.get(2,2) == -9.0);
			
			assertTrue("The item (0,0) should be 10.0.",
					matrix1.get(0,0) == 10.0);
			assertTrue("The item (0,1) should be 11.0.",
					matrix1.get(0,1) == 11.0);
			assertTrue("The item (0,2) should be 12.0.",
					matrix1.get(0,2) == 12.0);
			assertTrue("The item (1,0) should be 13.0.",
					matrix1.get(1,0) == 13.0);
			assertTrue("The item (1,1) should be 14.0.",
					matrix1.get(1,1) == 14.0);
			assertTrue("The item (1,2) should be 15.0.",
					matrix1.get(1,2) == 15.0);
			assertTrue("The item (2,0) should be 16.0.",
					matrix1.get(2,0) == 16.0);
			assertTrue("The item (2,1) should be 17.0.",
					matrix1.get(2,1) == 17.0);
			assertTrue("The item (2,2) should be 18.0.",
					matrix1.get(2,2) == 18.0);
		} catch (MatrixSizeException e) {
			fail();
		}
	}
	
	@Test
	public void test1by1ScalarMul() {
			RowMatrix matrix0 = new RowMatrix(1,1);
			matrix0.set(0, 0, 1.0);
			RowMatrix matrix1 = matrix0.mul(3.0);
			assertTrue("The item should be 1.0",
					matrix0.get(0, 0) == 1.0);
			assertTrue("The item should be 3.0",
					matrix1.get(0, 0) == 3.0);
	}
	
	@Test
	public void test1by1ScalarIMul() {
		RowMatrix matrix0 = new RowMatrix(1,1);
		matrix0.set(0, 0, 1.0);
		matrix0.imul(3.0);
		assertTrue("The item should be 3.0",
				matrix0.get(0, 0) == 3.0);
	}
	
	@Test
	public void test2by2ScalarMul() {

			RowMatrix matrix0 = new RowMatrix(2,2);
			matrix0.set(0, 0, 1.0);
			matrix0.set(0, 1, 2.0);
			matrix0.set(1, 0, 3.0);
			matrix0.set(1, 1, 4.0);
			
			RowMatrix matrix1 = matrix0.mul(3.0);
			
			assertTrue("The item (0,0) should be 1.0.",
					matrix0.get(0,0) == 1.0);
			assertTrue("The item (0,1) should be 2.0.",
					matrix0.get(0,1) == 2.0);
			assertTrue("The item (1,0) should be 3.0.",
					matrix0.get(1,0) == 3.0);
			assertTrue("The item (1,1) should be 4.0.",
					matrix0.get(1,1) == 4.0);
			
			assertTrue("The item (0,0) should be 3.0.",
					matrix1.get(0,0) == 3.0);
			assertTrue("The item (0,1) should be 6.0.",
					matrix1.get(0,1) == 6.0);
			assertTrue("The item (1,0) should be 9.0.",
					matrix1.get(1,0) == 9.0);
			assertTrue("The item (1,1) should be 12.0.",
					matrix1.get(1,1) == 12.0);
	}
	
	@Test
	public void test2by2ScalarIMul() {

			RowMatrix matrix0 = new RowMatrix(2,2);
			
			matrix0.set(0, 0, 1.0);
			matrix0.set(0, 1, 2.0);
			matrix0.set(1, 0, 3.0);
			matrix0.set(1, 1, 4.0);
			
			RowMatrix matrix1 = matrix0.mul(3.0);
			
			assertTrue("The item (0,0) should be 3.0.",
					matrix1.get(0,0) == 3.0);
			assertTrue("The item (0,1) should be 6.0.",
					matrix1.get(0,1) == 6.0);
			assertTrue("The item (1,0) should be 9.0.",
					matrix1.get(1,0) == 9.0);
			assertTrue("The item (1,1) should be 12.0.",
					matrix1.get(1,1) == 12.0);
	}
	
	@Test
	public void test3by3ScalarMul() {

			RowMatrix matrix0 = new RowMatrix(3,3);
			
			matrix0.set(0, 0, 1.0);
			matrix0.set(0, 1, 2.0);
			matrix0.set(0, 2, 3.0);
			matrix0.set(1, 0, 4.0);
			matrix0.set(1, 1, 5.0);
			matrix0.set(1, 2, 6.0);
			matrix0.set(2, 0, 7.0);
			matrix0.set(2, 1, 8.0);
			matrix0.set(2, 2, 9.0);
			
			RowMatrix matrix1 = matrix0.mul(3.0);
			
			assertTrue("The item (0,0) should be 1.0.",
					matrix0.get(0,0) == 1.0);
			assertTrue("The item (0,1) should be 2.0.",
					matrix0.get(0,1) == 2.0);
			assertTrue("The item (0,2) should be 3.0.",
					matrix0.get(0,2) == 3.0);
			assertTrue("The item (1,0) should be 4.0.",
					matrix0.get(1,0) == 4.0);
			assertTrue("The item (1,1) should be 5.0.",
					matrix0.get(1,1) == 5.0);
			assertTrue("The item (1,2) should be 6.0.",
					matrix0.get(1,2) == 6.0);
			assertTrue("The item (2,0) should be 7.0.",
					matrix0.get(2,0) == 7.0);
			assertTrue("The item (2,1) should be 8.0.",
					matrix0.get(2,1) == 8.0);
			assertTrue("The item (2,2) should be 9.0.",
					matrix0.get(2,2) == 9.0);
			
			assertTrue("The item (0,0) should be 3.0.",
					matrix1.get(0,0) == 3.0);
			assertTrue("The item (0,1) should be 6.0.",
					matrix1.get(0,1) == 6.0);
			assertTrue("The item (0,2) should be 9.0.",
					matrix1.get(0,2) == 9.0);
			assertTrue("The item (1,0) should be 12.0.",
					matrix1.get(1,0) == 12.0);
			assertTrue("The item (1,1) should be 15.0.",
					matrix1.get(1,1) == 15.0);
			assertTrue("The item (1,2) should be 18.0.",
					matrix1.get(1,2) == 18.0);
			assertTrue("The item (2,0) should be 21.0.",
					matrix1.get(2,0) == 21.0);
			assertTrue("The item (2,1) should be 24.0.",
					matrix1.get(2,1) == 24.0);
			assertTrue("The item (2,2) should be 27.0.",
					matrix1.get(2,2) == 27.0);
	}
	
	@Test
	public void test3by3ScalarIMul() {
			RowMatrix matrix0 = new RowMatrix(3,3);
			
			matrix0.set(0, 0, 1.0);
			matrix0.set(0, 1, 2.0);
			matrix0.set(0, 2, 3.0);
			matrix0.set(1, 0, 4.0);
			matrix0.set(1, 1, 5.0);
			matrix0.set(1, 2, 6.0);
			matrix0.set(2, 0, 7.0);
			matrix0.set(2, 1, 8.0);
			matrix0.set(2, 2, 9.0);
			
			matrix0.imul(3.0);
			
			assertTrue("The item (0,0) should be 3.0.",
					matrix0.get(0,0) == 3.0);
			assertTrue("The item (0,1) should be 6.0.",
					matrix0.get(0,1) == 6.0);
			assertTrue("The item (0,2) should be 9.0.",
					matrix0.get(0,2) == 9.0);
			assertTrue("The item (1,0) should be 12.0.",
					matrix0.get(1,0) == 12.0);
			assertTrue("The item (1,1) should be 15.0.",
					matrix0.get(1,1) == 15.0);
			assertTrue("The item (1,2) should be 18.0.",
					matrix0.get(1,2) == 18.0);
			assertTrue("The item (2,0) should be 21.0.",
					matrix0.get(2,0) == 21.0);
			assertTrue("The item (2,1) should be 24.0.",
					matrix0.get(2,1) == 24.0);
			assertTrue("The item (2,2) should be 27.0.",
					matrix0.get(2,2) == 27.0);
	}
	

	
}
