/* Copyright (C) 2013, Cameron White
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package edu.pdx.cawhite.math.coordinates;

import edu.pdx.cawhite.math.ColumnMatrix;
import edu.pdx.cawhite.math.LengthException;
import edu.pdx.cawhite.iterators.IteratorException;

/**
 * @author Cameron Brandon White
 */
public class CoordinateMatrix extends ColumnMatrix {

	/**
	 * Create the matrix with the given items
	 * 
	 * @param items items to add to the matrix
	 */
	public CoordinateMatrix(double[][] components) {
		super(components);
	}

	/**
	 * Create a matrix with the given number of rows and columns
	 * 
	 * @param numberOfRows The number of rows the new matrix should have.
	 * @param numberOfCols The number of columns the new matrix should have.
	 */
	public CoordinateMatrix(int numberOfRows, int numberOfColumns) {
		super(numberOfRows - 1, numberOfColumns);
	}

	/**
	 * Get the item at the given position.
	 * 
	 * @param rowIndex 		The row index.
	 * @param columnIndex 	The column index.
	 * @return The item at the given position.
	 */
	public Double get(int rowIndex, int columnIndex)
			throws IndexOutOfBoundsException {
		return getCoordinate(columnIndex).get(rowIndex);
	}

	public Coordinate getCoordinate(int index) 
			throws IndexOutOfBoundsException  {

		Coordinate newCoordinate = null;	

		try {
			newCoordinate = new Coordinate(components[index]);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new IndexOutOfBoundsException();
		} catch (LengthException e) {
			assert false : "Fatal programming error";
		}

		return newCoordinate;
	}

	public CoordinateMatrix.Iterator getIterator() {
		return this.new Iterator();
	}

	/**
	 * Get the number of rows the matrix has.
	 * 
	 * @return the number of rows in the matrix
	 */
	public int getNumberOfRows() {
		return super.getNumberOfRows() + 1;
	}

	public void set(int rowIndex, int columnIndex, double value) 
			throws IndexOutOfBoundsException {
		getCoordinate(columnIndex).set(rowIndex, value);
	}

	public void setCoordinate(int index, Coordinate newCoordinate) 
			throws LengthException {
		Coordinate currentCoordinate = this.getCoordinate(index);
		currentCoordinate.copy(newCoordinate);
	}

		
	public class Iterator extends ColumnMatrix.Iterator {

		public Iterator() {
			this.index = 0;
		}

		public Coordinate get() throws IteratorException {
			try {
				return getCoordinate(index);
			} catch (IndexOutOfBoundsException e) {
				throw new IteratorException();
			}
		}

		public void set(Coordinate value) 
				throws IteratorException, LengthException {
			try {
				setCoordinate(index, value);
			} catch (IndexOutOfBoundsException e) {
				throw new IteratorException();
			}
		}
	}
}