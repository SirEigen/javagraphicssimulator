/* Copyright (C) 2013, Cameron White
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package edu.pdx.cawhite.math.transformations;

import edu.pdx.cawhite.math.Vector;
import edu.pdx.cawhite.math.RowMatrix;
import edu.pdx.cawhite.math.LengthException;
import edu.pdx.cawhite.math.MatrixSizeException;
import edu.pdx.cawhite.math.coordinates.Coordinate;
import edu.pdx.cawhite.math.coordinates.CoordinateMatrix;
import edu.pdx.cawhite.iterators.IteratorException;

/**
 * @author Cameron Brandon White
 */
public class TransformationMatrix extends RowMatrix {

	/**
	 * Create the matrix with the given items
	 * 
	 * @param items items to add to the matrix
	 */
	public TransformationMatrix(double[][] components) {
		super(components);
	}

	/**
	 * Create a matrix with the given number of rows and columns
	 * 
	 * @param numberOfRows The number of rows the new matrix should have.
	 * @param numberOfCols The number of columns the new matrix should have.
	 */
	public TransformationMatrix(int numberOfRows, int numberOfColumns) {
		super(numberOfRows, numberOfColumns);
	}
	
	/**
	 * Add this matrix with the other in a new matrix
	 * 
	 * @param other 	The other matrix
	 * @return 			The new matrix.
	 */
	public TransformationMatrix add(TransformationMatrix other) 
			throws MatrixSizeException {
		TransformationMatrix newMatrix = 
			new TransformationMatrix(getNumberOfRows(), getNumberOfColumns());
		return (TransformationMatrix) super.add(other, newMatrix);
	}

	/**
	 * Add this matrix with the other in place.
	 * 
	 * @param other 	The other matrix.
	 * @return 			This matrix modified.
	 */
	public TransformationMatrix iadd(TransformationMatrix other) 
			throws MatrixSizeException {
		return (TransformationMatrix) super.iadd(other);
	}

	/**
	 * Subtract this matrix from the other in a new matrix.
	 * 
	 * @param other The other matrix
	 * @return 		The new matrix.
	 */
	public TransformationMatrix sub(TransformationMatrix other) 
			throws MatrixSizeException {
		TransformationMatrix newMatrix = 
			new TransformationMatrix(getNumberOfRows(), getNumberOfColumns());
		return (TransformationMatrix) super.sub(other, newMatrix);
	}

	/**
	 * Subtract this matrix from the other in place.
	 * 
	 * @param other 	The other matrix.
	 * @return 			This matrix modified.
	 */
	public TransformationMatrix isub(TransformationMatrix other) 
			throws MatrixSizeException {
		return (TransformationMatrix) super.isub(other);
	}

	/**
	 * Multiply this matrix by the scalar in a new matrix.
	 * 
	 * @param scalar 	The scalar to multiply by.
	 * @return 			The new matrix.
	 */
	public TransformationMatrix mul(Double scalar) {
		TransformationMatrix newMatrix 
			= new TransformationMatrix(getNumberOfRows(), getNumberOfColumns());
		return (TransformationMatrix) mul(scalar, newMatrix);
	}

	/**
	 * Multiply this matrix by the scalar in place.
	 * 
	 * @param scalar 	The scalar to multiply by.
	 * @return 			This vector modified.
	 */
	public TransformationMatrix imul(Double scalar) {
		return (TransformationMatrix) super.imul(scalar);
	}

	/**
	 * Multiply this matrix by the other Matrix in a new matrix
	 * 
	 * @param other 	The other matrix.
	 * @return 			The new matrix.
	 */
	public CoordinateMatrix mul(CoordinateMatrix other) 
			throws MatrixSizeException {
		assert this.getNumberOfColumns() == other.getNumberOfRows();

		CoordinateMatrix newMatrix = new CoordinateMatrix(this.getNumberOfRows(),
											      		  other.getNumberOfColumns());

		return (CoordinateMatrix) super.mul(other, newMatrix);
	}

	/**
	 * Multiply this matrix by the other Matrix in a new matrix
	 * 
	 * @param other 	The other matrix.
	 * @return 			The new matrix.
	 */
	public CoordinateMatrix mul(CoordinateMatrix other, CoordinateMatrix newMatrix) 
			throws MatrixSizeException {
		return (CoordinateMatrix) super.mul(other, newMatrix);
	}

	/**
	 * Multiply this matrix by the other Matrix in a new matrix
	 * 
	 * @param other 	The other matrix.
	 * @return 			The new matrix.
	 */
	public Coordinate mul(Coordinate coordinate) throws LengthException {

		Coordinate newCoordinate = new Coordinate(coordinate.getLength());
		return (Coordinate) super.mul(coordinate, newCoordinate);
	}

	/**
	 * Multiply this matrix by the other Matrix in a new matrix
	 * 
	 * @param other 	The other matrix.
	 * @return 			The new matrix.
	 */
	public Coordinate mul(Coordinate coordinate, Coordinate newCoordinate) throws LengthException {

		return (Coordinate) super.mul(coordinate, newCoordinate);
	}

	public CoordinateMatrix pMul(CoordinateMatrix other) 
			throws MatrixSizeException {

		CoordinateMatrix newMatrix = new CoordinateMatrix(this.getNumberOfRows(),
											      		  other.getNumberOfColumns());

		return pMul(other, newMatrix);
	}
	

	/**
	 * Multiply this matrix by the other Matrix in a new matrix
	 * 
	 * @param other 	The other matrix.
	 * @return 			The new matrix.
	 */
	protected CoordinateMatrix pMul(CoordinateMatrix other, CoordinateMatrix newCoordinateMatrix) 
			throws MatrixSizeException {

		CoordinateMatrix.Iterator otherIterator = other.getIterator();
		CoordinateMatrix.Iterator newIterator   = newCoordinateMatrix.getIterator();

		while (true) {
			try {

				Coordinate newCoordinate = newIterator.get();
				Coordinate otherCoordinate = otherIterator.get();	

				pMul(otherCoordinate, newCoordinate);

				otherIterator.next();
				newIterator.next();

			} catch (IteratorException e) {
				break;
			} catch (LengthException e) {
				throw new MatrixSizeException();
			}
		}

		return newCoordinateMatrix;
	}

	/**
	 * Multiply this matrix by the other Matrix in a new matrix
	 * 
	 * @param other 	The other matrix.
	 * @return 			The new matrix.
	 */
	public Coordinate pMul(Coordinate vector, Coordinate newCoordinate) 
			throws LengthException {

		TransformationMatrix.Iterator matrixIterator = this.getIterator();
		Coordinate.Iterator vectorIterator = newCoordinate.getIterator();

		while (true) {
			try {

				Vector matrixVector = matrixIterator.get();
				
				if (vectorIterator.hasNext()) {
					vectorIterator.set(matrixVector.dot(vector));
				} else {
					double value = matrixVector.dot(vector);	
					newCoordinate.imul(1.0/value);
				}

				matrixIterator.next();
				vectorIterator.next();

			} catch (IteratorException e) {
				break;
			}
			
		}

		return newCoordinate;
	}

	/**
	 * Multiply this matrix by the other Matrix in a new matrix
	 * 
	 * @param other 	The other matrix.
	 * @return 			The new matrix.
	 */
	public Coordinate pMul(Coordinate coordinate) throws LengthException {

		Coordinate newCoordinate = new Coordinate(coordinate.getLength());
		return this.pMul(coordinate, newCoordinate);	
	}

	public TransformationMatrix matrixOperation(
			TransformationMatrix other, VectorOperation vectorOperation) 
			throws MatrixSizeException {

		TransformationMatrix newMatrix = new TransformationMatrix(getNumberOfRows(),
											getNumberOfColumns());

		return (TransformationMatrix) super.matrixOperation(other, newMatrix, vectorOperation);
	}

	public TransformationMatrix iMatrixOperation(
			TransformationMatrix other, IVectorOperation vectorOperation) 
			throws MatrixSizeException {

		return (TransformationMatrix) super.iMatrixOperation(other, vectorOperation);
	}

}