/* Copyright (C) 2013, Cameron White
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package edu.pdx.cawhite.simulator;

import java.awt.Canvas;

/**
 * @author Cameron Brandon White
 */
abstract class Screen<PLAYER_TYPE, OBJECT_TYPE, ENVIROMENT_TYPE> extends Canvas {

	private static final long serialVersionUID = 1L;

    public Boolean isUpPressed, isDownPressed, 
                   isLeftPressed, isRightPressed,
                   isTurnLeftPressed, isTurnRightPressed,
                   isRepaintNeeded;

    protected ENVIROMENT_TYPE enviroment;
    protected PLAYER_TYPE player;

    public Screen(ENVIROMENT_TYPE enviroment, PLAYER_TYPE player,
    			  int width, int length) {
    	this(width, length);
    	this.enviroment = enviroment;
        this.player = player;
    }

	protected Screen(int width, int length) {
		this();
		setSize(width, length);
	}

	protected Screen() {
		this.isUpPressed = false;
		this.isDownPressed = false;
		this.isLeftPressed = false;
		this.isRightPressed = false;
		this.isRepaintNeeded = false;
        this.isTurnLeftPressed = false;
        this.isTurnRightPressed = false;
        this.isRepaintNeeded = false;
	}

	public PLAYER_TYPE getPlayer() {
		return this.player;
	}

	public ENVIROMENT_TYPE getEnviroment() {
		return this.enviroment;
	}
}
