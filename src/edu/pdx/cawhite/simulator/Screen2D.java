/* Copyright (C) 2013, Cameron White
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package edu.pdx.cawhite.simulator;

import java.awt.Graphics;

import edu.pdx.cawhite.containers.DList;
import edu.pdx.cawhite.simulator.Enviroment2D;
import edu.pdx.cawhite.math.Vector;

/** 
 * @author Cameron Brandon White
 */
public class Screen2D extends Screen<Player2D, Object2D, Enviroment2D> implements Runnable {

    /**
     *
     */
    public Screen2D(Enviroment2D enviroment, Player2D player,
                    int width, int length) {

        super(enviroment, player, width, length);
    }

    /**
     *
     */
    public void run() {

        while(true) {
            if (isUpPressed) 
                translate(0.0, -0.1);
            if (isDownPressed) 
                translate(0.0, 0.1);
            if (isLeftPressed)
                translate(-0.1, 0.0);
            if (isRightPressed)
                translate(0.1, 0.0);
            if (isTurnLeftPressed)
                rotate(-0.1);
            if (isTurnRightPressed)
                rotate(0.1);
            if (isRepaintNeeded) {
                repaint();
                isRepaintNeeded = false;
            }
        }
    }

    /**
     *
     */
    public void paint(Graphics g) {
        player.paint(g);
        paintObjects(g);
    }

    /**
     *
     */
    protected void paintObjects(Graphics g) {
        Object2D object;
        DList<Object2D> list = enviroment.getObjects();
        DList<Object2D>.Iterator listIterator = list.new Iterator();

    }

    /**
     *
     */
    protected void translate(Double x, Double y) {
        Player2D player = getPlayer();
        player.translate(x, y);
        isRepaintNeeded = true;
    }

    /**
     *
     */
    protected void rotate(Double theta) {
        Player2D player = getPlayer();
        player.rotate(theta);
        isRepaintNeeded = true;
    }
}
