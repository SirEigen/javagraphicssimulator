/* Copyright (C) 2013, Cameron White
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package edu.pdx.cawhite.simulator;

import edu.pdx.cawhite.math.coordinates.Coordinate;
import edu.pdx.cawhite.math.ColumnMatrix;
import edu.pdx.cawhite.math.Vector;
import edu.pdx.cawhite.math.LengthException;

/**
 * @author Cameron Brandon White
 */
public class Player2D extends Object2D {
    
    protected Screen2D screen;

    /**
     * Create a new 2D player.
     *
     * @param enviroment    The environment to add the player to.
     * @param items         The items.
     */
    public Player2D(Enviroment2D enviroment, Coordinate center, Double size) 
            throws LengthException {
        super(center, size);
        this.enviroment = enviroment;
        createScreen();
        this.enviroment.addPlayer(this);
    }

    private void createScreen() {
        screen = new Screen2D(enviroment, this, 1000, 1000);
    }

    public Screen2D getScreen() {
        return screen;
    }

    public void setScreen(Screen2D screen) {
        this.screen = screen;
    }
}
