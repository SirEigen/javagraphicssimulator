/* Copyright (C) 2013, Cameron White
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package edu.pdx.cawhite.containers;

import edu.pdx.cawhite.iterators.BidirectionalIterator;
import edu.pdx.cawhite.iterators.IteratorException;

/**
 * Abstract List implementing a List.
 *
 * The abstract List has takes two generics NODE_TYPE and ITEM_TYPE.
 * This allows any class that extends List to choose what type of node
 * and item it contains.
 * 
 * @see edu.pdx.cawhite.containers.DList
 *
 * @author Cameron Brandon White
 */
public abstract class List<NODE_TYPE, ITEM_TYPE> extends Object{
    // The current number of nodes/items in the list.
    protected int length;
    
    protected NODE_TYPE front, back;

    /**
     * Initialize the List.
     */
    public List() {
        super();
        length = 0;
    }
    
    /** 
     * Get the number of items in the list.
     *
     * @return The number of items in the List. 
     */
    public int getLength() {
        return length;
    }

    public abstract void append(ITEM_TYPE item);
    public abstract void prepend(ITEM_TYPE item);
    public abstract void insert(ITEM_TYPE item, int afterIndex);
    public abstract void delete(int index);
    public abstract ITEM_TYPE get(int index);
    public abstract void set(int index, ITEM_TYPE item);
    
    public abstract class Iterator extends BidirectionalIterator {
        
        // The index of the currently corresponding to the
        // currently selected node (e.g currentNode)
        protected int currentIndex;

        // The currently referenced node.
        protected NODE_TYPE currentNode;

        public abstract ITEM_TYPE get() throws IteratorException;
        public abstract void set(ITEM_TYPE value) throws IteratorException;
    }
}
