/* Copyright (C) 2013, Cameron White
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package edu.pdx.cawhite.containers;

import edu.pdx.cawhite.iterators.IteratorException;

/**
 * DList is a double linked List.
 * 
 * @author Cameron Brandon White
 */
public class DList<T> extends List<DList<T>.DNode, T> {

	protected DNode front, back;

	public DList() {
		super();
		front = null;
		back = null;
	}

	/**
	 * Add a item to the back of the list.
	 * 
	 * @param item 		The item to add.
	 */
	public void append(T item) {
		insert(item, length - 1);
	}

	/**
	 * Add a item to the front of the list.
	 * 
	 * @param item 	The item to add.
	 */
	public void prepend(T item) {
		insert(item, -1);
	}

	/**
	 * Insert the item after the given index.
	 * 
	 * @param item 			The item to add.
	 * @param afterIndex 	The index the new item will come after.
	 */
	public void insert(T item, int afterIndex) {
		assert -1 <= afterIndex && afterIndex < length;

		if (afterIndex == -1) {

			if (front != null)
				front.previous = front;

			front = new DNode(item, null, front);

			if (back == null)
				back = front;

		} else {

			DNode previousNode = getNode(afterIndex);
			DNode nextNode = (previousNode != null) ? previousNode.next : null;
			DNode newNode = new DNode(item, previousNode, nextNode);

			if (previousNode == null)
				this.front = newNode;
			else
				previousNode.next = newNode;

			if (nextNode == null)
				this.back = newNode;
			else
				nextNode.previous = newNode;
		}

		length++;
	}

	/**
	 * Delete the item at the given index.
	 * 
	 * @param index 	The index of the item to be deleted.
	 */
	public void delete(int index) {
		assert 0 <= index && index < length;

		DNode previousNode = (index - 1 >= 0) ? getNode(index - 1) : null;
		DNode deleteNode = (previousNode != null) ? previousNode.next : getNode(index);
		DNode nextNode = (deleteNode != null) ? deleteNode.next : null;

		if (previousNode != null)
			previousNode.next = nextNode;

		if (nextNode != null)
			nextNode.previous = previousNode;

		if (index == 0)
			this.front = nextNode;

		if (index == length - 1)
			this.back = previousNode;

		length--;
	}

	/**
	 * Get the item at the index.
	 * 
	 * @param index The index of the item wanted.
	 * @return 		The item at the given index.
	 */
	public T get(int index) {
		return getNode(index).get();
	}

	/**
	 * Set the item at the index.
	 * 
	 * @param index 	The index of the item you want to set.
	 * @param item 		The new item.
	 */
	public void set(int index, T item) {
		getNode(index).set(item);
	}

	public DNode getFront() {
		return this.front;
	}

	public DNode getBack() {
		return this.back;
	}

	/**
	 * Get the node at the given index
	 * 
	 * @param index 	The index of the wanted node.
	 * @return 			The node at the wanted index.
	 */
	private DNode getNode(int index) {
		assert length + index >= 0 && length + index < 2 * length;

		DNode selectedNode;

		if (index >= 0) {

			selectedNode = this.front;
			for (int i = index; i > 0; i--)
				selectedNode = selectedNode.next;

		} else {

			selectedNode = this.back;
			for (int i = length - 1; i >= length - index; i--)
				selectedNode = selectedNode.previous;
		}

		return selectedNode;
	}

	/**
	 * @author Cameron Brandon White
 	 */
	public class DNode extends Node<T> {

		private DNode previous;
		private DNode next; 

		/**
		 * Create an non-empty node that points to the given.
		 * 
		 * @param item 		The item to place in the node.
		 * @param previous 	The previous node.
		 * @param next 		The next node.
		 */
		public DNode(T item, DNode previous, DNode next) {
			super(item);
			this.previous = previous;
			this.next = next;
		}
	}

	/**
	 * @author Cameron Brandon White
 	 */
	public class Iterator extends List<DNode, T>.Iterator {

		// The index of the currently corresponding to the
		// currently selected node (e.g currentNode)
		protected int currentIndex;

		// The currently referenced node.
		protected DNode currentNode;

		/**
		 * Construct a iterator for the list.
		 * 
		 * @param list 	The list to iterate over.
		 */
		public Iterator() {
			this.currentIndex = 0;
			this.currentNode = front;
		}

		/**
		 * Get the item in the current node.
		 * 
		 * @return The item in the current node.
		 */
		public T get() throws IteratorException {
			if (currentNode != null)
				return currentNode.get();
			else
				throw new IteratorException();
		}

		/**
		 * Set the item in the current node.
		 * 
		 * @param item 	The new item to put in the node.
		 */
		public void set(T item) throws IteratorException {
			if (currentNode != null)
				currentNode.set(item);
			else
				throw new IteratorException();
		}

		/** Move the iterator back one node. */
		public void previous() throws IteratorException {
			if (currentNode != null) {
				currentIndex++;
				currentNode = currentNode.previous;
			} else
				throw new IteratorException();
		}

		/** Move the iterator forward one node. */
		public void next() throws IteratorException {
			if (currentNode != null) {
				currentIndex++;
				currentNode = currentNode.next;
			} else
				throw new IteratorException();
		}

		public Boolean hasNext() {
			if (currentNode != null && currentNode.next != null)
				return true;
			else
				return false;
		}
		
		public Boolean hasPrevious() {
			if (currentNode != null && currentNode.previous != null)
				return true;
			else
				return false;
		}

		public void restart() {
			this.currentNode = front;
			currentIndex = 0;	
		}
	}
}